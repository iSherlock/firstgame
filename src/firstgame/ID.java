package firstgame;

public enum ID {
    Player(),
    Enemy(),
    Bullet(),
    Collectable();
}