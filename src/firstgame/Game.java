package firstgame;

import java.awt.Canvas;
import java.awt.image.BufferStrategy;
import java.awt.Graphics;
import java.awt.Color;



public class Game extends Canvas implements Runnable{


    /**
     *
     */
    private static final long serialVersionUID = 1116661633092004215L;

    public static final int WIDTH = 800, HEIGHT = 600;

    private Thread thread;
    private boolean running = false;

    private Handler handler;

    private HUD hud;

    public Game(){
        handler = new Handler();
        this.addKeyListener(new KeyInput(handler));

        
        
        new Window(WIDTH, HEIGHT, "Gungeon Rip Off", this);

        hud = new HUD();
        

        handler.addObject(new Player(WIDTH/2-32,HEIGHT/2-32, ID.Player, handler));
        handler.addObject(new BasicEnemy(200, 200, ID.Enemy));
    }

    public static void main(String[] args) {
        new Game();
    }


    public synchronized void start(){
        thread = new Thread(this);
        thread.start();
        running = true;
    }

    public synchronized void stop(){
        try {
            thread.join();
            running = false;
        } catch (Exception e) {
            e.printStackTrace();
        }
        thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() {
        this.requestFocus();
        long lastTime = System.nanoTime();
        double amountOfTicks = 60.0;
        double ns = 1000000000 / amountOfTicks;
        double delta = 0;
        long timer = System.currentTimeMillis();
        int frames = 0;
        while(running){
            long now = System.nanoTime();
            delta += (now - lastTime) / ns;
            lastTime = now;
            while(delta >= 1){
                tick();
                delta--;
            }
            if(running)
                render();
            frames++;

            if(System.currentTimeMillis() - timer > 1000){
                timer += 1000;
                System.out.println("FPS: " + frames);
                frames = 0;
            }
        }
        stop();
    }

    private void tick(){
        handler.tick();
        hud.tick();
    }

    private void render(){
        BufferStrategy bs = this.getBufferStrategy();
        if(bs == null){
            this.createBufferStrategy(3);
            return;
        }
        Graphics g = bs.getDrawGraphics();


        g.setColor(Color.black);
        g.fillRect(0, 0, WIDTH , HEIGHT);

        handler.render(g);

        hud.render(g);

        g.dispose();
        bs.show();


    }

    public static int clamp(int val, int min, int max){
        if(val >= max)
            return val = max;
        else if(val <= min)
            return val = min;
        else return val;
    }

// TODO make good game
    
}